from PIL import Image, ImageFont, ImageDraw
from os.path import join, dirname, abspath


class Uplatnica:
    SAMPLE = join(join(dirname(dirname(abspath(__file__))), 'assets'), 'uplatnica.png')

    __slots__ = ['uplatilac', 'adresa_uplatioca',
                 'svrha_uplate',
                 'primalac', 'adresa_primaoca',
                 'sifra_placanja', 'valuta', 'iznos',
                 'racun_primaoca',
                 'broj_modela', 'poziv_na_broj',

                 '_fsmall', '_fbig']

    def __init__(self, font='arial.ttf'):
        try:
            self._fsmall = ImageFont.truetype(font, 18)
            self._fbig = ImageFont.truetype(font, 28)
        except OSError:
            raise Exception("{} unavailable, init with font name as argument -> "
                            "Uplatnica(_fsmallname)".format(font))

        self.uplatilac = self.adresa_uplatioca = self.svrha_uplate = self.primalac = \
            self.adresa_primaoca = self.sifra_placanja = self.valuta = self.iznos = \
            self.racun_primaoca = self.broj_modela = self.poziv_na_broj = ''

    def _render(self):
        i = Image.open(Uplatnica.SAMPLE)
        draw = ImageDraw.Draw(i)

        draw.text((65, 65), '{}\n{}'.format(self.uplatilac, self.adresa_uplatioca), (0, 0, 0), font=self._fsmall)
        draw.text((65, 190), self.svrha_uplate, (0, 0, 0), font=self._fsmall, size=150)
        draw.text((65, 315), '{}\n{}'.format(self.primalac, self.adresa_primaoca), (0, 0, 0), font=self._fsmall)

        draw.text((695, 95), self.sifra_placanja, (0, 0, 0), font=self._fbig)
        draw.text((800, 95), self.valuta, (0, 0, 0), font=self._fbig)
        draw.text((920, 95), "{:0,.2f}".format(self.iznos), (0, 0, 0), font=self._fbig)
        draw.text((800, 165), self.racun_primaoca, (0, 0, 0), font=self._fbig)
        draw.text((695, 240), self.broj_modela, (0, 0, 0), font=self._fbig)
        draw.text((800, 238), self.poziv_na_broj, (0, 0, 0), font=self._fbig)

        return i

    def save(self, dest, ext='PNG'):
        self._render().save(dest, ext)
        return dest


def generate_receipt(delegate):
    r = Uplatnica()

    r.uplatilac = '{d.name} {d.surname}'.format(d=delegate)
    r.svrha_uplate = 'Kotizacija za 109-u Nacionalnu konferenciju'
    r.primalac = 'AIESEC Lokalni komitet Niš'
    r.adresa_primaoca = 'Trg Kralja Aleksandra 11, Niš'

    r.sifra_placanja = '189'
    r.valuta = 'RSD'
    r.iznos = delegate.total_sum
    r.racun_primaoca = '160-129610-22'
    r.poziv_na_broj = delegate.public_id

    return r
