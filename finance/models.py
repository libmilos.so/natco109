from django.db import models
from django.db import transaction
from django.conf import settings

from sequences import get_next_value

from .utils import generate_invoice
from public.models import Delegate
from public.utils import timezone


class Invoice(models.Model):
    number = models.IntegerField()
    to = models.ForeignKey(Delegate)
    intl = models.BooleanField(default=False)
    round = models.IntegerField()
    split = models.BooleanField()
    created_on = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def get_or_create(delegate):
        try:
            inv = Invoice.objects.get(to=delegate)
            # return file
        except Invoice.DoesNotExist:
            inv = Invoice(to=delegate)
            inv.intl = (delegate.dtype == Delegate.INTL)
            inv.round = None # TODO: CALCULATE FROM CONFIG FILE

            with transaction.atomic():
                # TODO: On error, mail admin immediately
                inv.number = get_next_value('invoice.2017.natco')
                fil = generate_invoice(delegate, inv.number)
                # if delegate.split_sum:
                with open(settings.INVOICE_DIR, "w") as f:
                    f.write(fil)
                inv.save()
