from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse

from .utils import generate_receipt, generate_invoice
from public.models import Delegate


def receipt(request, id):
    """
    Returns generated receipt PNG for the specified delegate.
    TODO: Save to file and pull when needed.
    """
    app = get_object_or_404(Delegate, public_id=id)

    receipt = generate_receipt(app)
    response = HttpResponse(content_type="image/png")

    return receipt.save(response)


def invoice(request, id):
    """
    Returns generated invoice in PDF.
    TODO: Save to file and pull when needed.
    """
    app = get_object_or_404(Delegate, public_id=id)

    invoice = generate_invoice(app)
    response = HttpResponse(content_type="application/pdf")
    response['Content-Disposition'] = 'filename="%s-%spdf"' % (app.name, app.surname)

    response.write(invoice)
    return response
