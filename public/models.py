from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse

import datetime
import uuid
from decimal import Decimal

from finance.models import Invoice


"""Populate MC and LC by running ./manage.py getexpadata"""

class MC(models.Model):
    SERBIA = 1547 # ID from expa

    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)


class LC(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    mc = models.ForeignKey(MC)

    @staticmethod
    def get_active_LC_ids(names=False):
        # I know this is an ugly fucking ass way to do this,
        # but there is no point in putting an 'active' column
        # in the db
        lcs = {
            1235: 'Niš',
            1349: 'EF',
            1195: 'FON',
            191:  'MET',
            487:  'KG',
            1517: 'SINGI',
            1279: 'NS',
            84:   'Valjevo',
        }

        if names:
            return lcs
        return tuple(lcs)

    @staticmethod
    def get_serbian_LC_ids(names=False):
        # All Serbian LCs ever
        # TODO: Add old LCs: Subotica, BBA, etc.
        lcs = LC.get_active_LC_ids(True)

        vanity = {
            # VANITY IDs. POPULATE FIRST WITH ./manage.py initlegacylcs
            10001: 'ALFA',
            10002: 'BK',
            10003: 'BBA',
            10004: 'Subotica',
        }

        lcs.update(vanity)

        if names:
            return lcs
        return tuple(lcs)



class Delegate(models.Model):
    # Delegate types
    STANDARD = 1
    INTL = 2
    PARTY = 3

    # Common
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    gender = models.BooleanField() # male: 0, female: 1
    dob = models.DateField()
    phone = models.CharField(max_length=30)
    email = models.EmailField(unique=True)
    fb = models.CharField(max_length=100)
    emergency = models.CharField(max_length=300)
    lc = models.ForeignKey(LC)
    food_req = models.CharField(blank=True, max_length=300)
    allergies = models.CharField(blank=True, max_length=300)
    expectations = models.TextField()
    comments = models.CharField(blank=True, max_length=500)
    
    arrival = models.DateField()
    departure = models.DateField()
    gala = models.BooleanField()

    # Normal + Intl
    uni = models.CharField(blank=True, max_length=200)
    track = models.IntegerField(blank=True, null=True)
    position = models.CharField(blank=True, max_length=100)
    transport = models.BooleanField(blank=True, default=False)
    tshirt = models.IntegerField(blank=True, null=True)

    # Intl
    # Serbian ID is 1547, default because most delegates will be national ones
    mc = models.ForeignKey(MC, default=MC.SERBIA)
    place_arr_dep = models.CharField(blank=True, max_length=150)
    pre_conf_accommodation = models.BooleanField(blank=True, default=False)

    # Party
    attendance_count = models.IntegerField(blank=True, default=0)


    # Meta info
    dtype = models.IntegerField(default=1) # 1: normal, 2: intl, 3: party
    
    applied_ip = models.GenericIPAddressField()
    applied_on = models.DateTimeField(auto_now_add=True)

    confirmed_ip = models.GenericIPAddressField(null=True, default=None)
    confirmed_on = models.DateTimeField(null=True)

    public_id = models.CharField(unique=True, max_length=20) #format: %(LC_ID)s-%(5_RANDOM_CHARS)s
    auth_code = models.UUIDField(default=uuid.uuid4)

    #invoice = models.OneToOneField(Invoice)
    total_sum = models.DecimalField(max_digits=10, decimal_places=2)
    paid_sum = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal('0.00'))


    def confirm(self, ip, save=False):
        self.confirmed_on = datetime.datetime.now()
        self.confirmed_ip = ip
        if save:
            self.save()


    def save(self, *args, **kwargs):
        if not self.public_id:
            # format: %(LC_ID)s-%(5_RANDOM_CHARS)s
            self.public_id = "%s-%s" % (self.lc.id, uuid.uuid4().hex[::-1][:5]) #whatever lol
        super(Delegate, self).save(*args, **kwargs)


    def has_paid(self):
        return self.paid_sum >= self.total_sum


    def nights_booked(self):
        return (self.departure - self.arrival).days


    def track_name(self):
        return {
            0: 'LCP',
            1: 'EB',
            2: 'TLP',
            3: 'TMP',
            4: 'NLDS',
            5: 'Other',
        }[self.track]

    def get_absolute_url(self, auth=None):
        kwargs = {'id': self.public_id}
        if auth:
            kwargs['auth'] = str(self.auth_code)

        return reverse('app', kwargs=kwargs)

    def get_absolute_auth_url(self):
        return self.get_absolute_url(True)

    @staticmethod
    def calculate_price(dtype, check_in, check_out, gala, tshirt):
        """
        dtype: int - delegate type (standard: 1, intl: 2, party: 3)
        check_in, check_out: datetime.date
        gala, tshirt: bool

        Doesn't validate dates, make sure you supply valid ones.
            (whether supplied days are within conference days,
            check_out date is not before check_in date)
        """
        if dtype not in (Delegate.STANDARD, Delegate.INTL, Delegate.PARTY):
            raise ValueError("Invalid delegate type")

        price = 0
        # OC fee
        if dtype == Delegate.STANDARD:
            price += settings.PRICING['fee']
        elif dtype == Delegate.INTL:
            price += settings.PRICING['fee'] + settings.PRICING['intladdedfee']
        elif dtype == Delegate.PARTY:
            price += settings.PRICING['partyfee']

        # Accommodation price (paid per night)
        price += (check_out - check_in).days * settings.PRICING['accommodation']

        if gala:
            price += settings.PRICING['gala']
        if tshirt:
            price += settings.PRICING['tshirt']

        return price


    @staticmethod
    def by_public_id(id):
        #try:
            return Delegate.objects.get(public_id=id) #?
        #except Delegate.DoesNotExist:
        #    return None # or raise?


"""
TODO:
- access logging
- intrusion logging?
"""
