from django.conf import settings
from django.utils import timezone


def round_by_date(date):
    if timezone.datetime(*settings.ROUNDS[0]['start']) < date:
        round = -1 # First round not opened yet
    elif timezone.datetime(*settings.ROUNDS[-1]['end']) > date:
        round = -2 # Last round closed
    else:
        for rnd in settings.ROUNDS[1:-1]:
            if timezone.datetime(*rnd['start']) < date < timezone.datetime(*rnd['stop']):
                round = rnd['count']
    return round

def current_round():
    return round_by_date(timezone.now())
