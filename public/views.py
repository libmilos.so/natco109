from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect

import json
import decimal

from .forms import (
    resolver,

    StandardDelegateForm,
    IntlDelegateForm,
    PartyDelegateForm,
)
from .models import Delegate, LC

dtypes = {
    'standard': 1,
    'intl': 2,
    'party': 3
}

def index(request):
    """
    Path: /

    Home page.
    """
    lc_list = LC.objects.order_by('name').all()
    data = {'lc_list': lc_list}
    return render(request, 'public/index.html', data)


def _processApply(request):
    # Applications are disabled right now
    raise Http500()

    try:
        dtype = int(request.POST.get('dtype'))
    except ValueError:
        # Faster than isdigit()
        # Change this to gracefully return Http500
        raise Exception("HTTP CODE 500")

    if dtype not in resolver:
        # This shouldn't happen unless
        # someone is sending a tampered request
        return HttpResponseRedirect(reverse('apply'))

    form = resolver[dtype](request.POST)

    if form.is_valid():
        # We don't commit immediately,
        # as we got more work to do before saving it
        delegate = form.save(commit=False)

        delegate.dtype = dtype
        delegate.applied_ip = request.META['REMOTE_ADDR']

        total_sum = Delegate.calculate_price(
            dtype,
            form.cleaned_data['arrival'],
            form.cleaned_data['departure'],
            form.cleaned_data['gala'],
            form.cleaned_data.get('tshirt', False),
        )
        delegate.total_sum = decimal.Decimal(total_sum)

        delegate.save()

        return (True, form, delegate)
    else:
        return (False, form, None)

def apply(request):
    """
    Path: /apply

    On GET, this view serves as a static page application for
    clients without javascript or slow phones.

    On POST, it serves to save new applies.
    Afterwards, redirects to /app/:userID:
    """
    if request.method == 'GET':
        # Static application form
        # - slower mobile devices
        # - clients without JS
        data = {
            'form': None,
            'default': None,
        }
        return render(request, 'public/static_application.html', data)


    if request.method == 'POST':
        # Someone is applying for NatCo! Yay!!!
        (success, form, delegate) = _processApply(request)

        if request.is_ajax():
            # AJAX form submission
            if success:
                reply = {'success': True, 'redir': reverse('app', kwargs={'id': delegate.public_id})}
            else:
                reply = {'success': False, 'errors': form.errors}
            response = JsonResponse(reply)

        else:
            # Static form submission
            if success:
                # Redirect them to app page
                response = HttpResponseRedirect(reverse('app', kwargs={'id': delegate.public_id}))
            else:
                data = {
                    'form': form,
                    'default': form.type,
                }
                response = render(request, 'public/static_application.html', data)


        if success:
            # TODO: Send mail
            pass

        return response



def application(request, id, auth=None):
    """
    TODO:
    - if wrong auth_code, log and alert admin
    """
    id = id.lower()
    if auth:
        auth = auth.lower()

    app = get_object_or_404(Delegate, public_id=id)

    data = {
        'app': app,
    }

    if not auth:
        return HttpResponse("We're so happy to see you apply for NatCo 109th!"
            " We'll contact you in a few days with more info, so do check your mail! :)")

    if str(app.auth_code).replace('-', '') == auth.lower():
        return render(request, 'public/dashboard.html', data)



def action(request, auth_code):
    """
    Same URL as application view but POST?
    """
    raise NotImplementedError


def list_lcs(request):
    """
    Returns list of all LCs from database.
    Format:
    {
        "payload": [
            [name, id],
            [name, id],
            ...
        ]
    }
    """
    data = [(lc.name, lc.id) for lc in LC.objects.all()]
    return JsonResponse({'payload': data})
