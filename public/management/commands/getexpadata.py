from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

import requests

from public.models import LC, MC

class Command(BaseCommand):
    help = 'Get entity/LC data from expa'
    token = 'e316ebe109dd84ed16734e5161a2d236d0a7e6daf499941f7c110078e3c75493'

    def handle(self, *args, **options):
        self.stdout.write('Fetching data from EXPA... ', ending='')
        data = requests.get("https://gis-api.aiesec.org:443/v1/lists/offices.json?"
                            "access_token=%s" % self.token).json()
        self.stdout.write('OK')

        self.stdout.write('Writing data to database...')
        with transaction.atomic():
            for entity in data:
                if not entity['suboffices']:
                    continue
                mc = MC()
                mc.id = entity['id']
                mc.name = entity['name']
                mc.save()

                if options['verbosity'] > 1:
                    self.stdout.write(' {0}'.format(mc.name))

                for committee in entity['suboffices']:
                    if 'closed' in committee['name'].lower() or 'closed' in committee['full_name'].lower():
                        continue
                    lc = LC()
                    lc.id = committee['id']
                    lc.name = committee['name']
                    lc.mc = mc
                    lc.save()

                    if options['verbosity'] > 2:
                        self.stdout.write(' - {0}'.format(lc.name))

        self.stdout.write('Job well done!')
