from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from public.models import LC, MC

class Command(BaseCommand):
    help = 'Add historic LCs to database (used for party delegates)'

    def handle(self, *args, **options):
        srb = MC.objects.filter(id=MC.SERBIA).first()
        if not srb:
            raise CommandError("Expa data not initialized. Run getexpadata command first.")

        for lc in LC.get_serbian_LC_ids(names=True).items():
            if lc[0] > 10000:
                LC(id=lc[0], name=lc[1], mc=srb).save()
