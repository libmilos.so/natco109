from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^apply$', views.apply, name='apply'),

    # Must be in this order as the 2nd one matches 1st one as well
    url(r'^app/(?P<id>.*)/(?P<auth>.*)$', views.application, name='app'),
    url(r'^app/(?P<id>.*)$', views.application, name='app'),

    url(r'^action$', views.action, name='action'),

    # Utils
    url(r'^list/lcs$', views.list_lcs, name='list-lcs'),
]
