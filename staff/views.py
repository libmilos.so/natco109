from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.utils import timezone
from django.http import JsonResponse

import datetime

from public.models import Delegate, LC, MC


# @login_required
def dashboard(request):
    # if request.GET.get('auth') != '1':
    #     raise PermissionDenied

    data = {
        'applies_today': Delegate.objects.filter(
            applied_on__gt=datetime.date.today()).count(),
        'applies_this_week': Delegate.objects.filter(
            applied_on__gt=datetime.date.today() - datetime.timedelta(days=7)).count(),
    }

    return render(request, 'staff/dashboard.html', data)

# @login_required
def apps(request):
    # data = {
    #     'apps_standard': Delegate.objects.filter(dtype=Delegate.STANDARD),
    #     'apps_intl': Delegate.objects.filter(dtype=Delegate.INTL),
    #     'apps_party': Delegate.objects.filter(dtype=Delegate.PARTY),
    # }
    data = {
        'apps': Delegate.objects.all(),
    }

    return render(request, 'staff/applications.html', data)








############
### AJAX ###
############

# @login_required
def stats_apply(request):
    ref = timezone.now().replace(hour=0, minute=0, second=0)
    dates = []
    count = []

    for i in range(6, -1, -1):
        date = ref - timezone.timedelta(days=i)
        cn = Delegate.objects.filter(
            applied_on__year=date.year,
            applied_on__month=date.month,
            applied_on__day=date.day,
        ).count()

        dates.append(date.strftime("%d.%m"))
        count.append(cn)

    data = {
        'dates': dates,
        'count': count,
    }
    return JsonResponse(data)

# @login_required
def stats_lc(request):
    data = []
    for lc in LC.objects.filter(pk__in=LC.get_active_LC_ids()):
        entry = {'label': lc.name}
        entry['value'] = lc.delegate_set.count()
        data.append(entry)

    return JsonResponse({'payload': data})
