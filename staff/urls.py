from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.dashboard, name='dashboard'),

    url(r'^applications$', views.apps, name='apps'),
    # url(r'^applications/standard$', views.apps_standard, name='apps.standard'),
    # url(r'^applications/intl$',     views.apps_intl,     name='apps.intl'),
    # url(r'^applications/party$',    views.apps_party,    name='apps.party'),

    # AJAX
    url(r'^stats/apply.json$', views.stats_apply, name='stats.apply'),
    url(r'^stats/lc.json$', views.stats_lc, name='stats.lc'),
]
